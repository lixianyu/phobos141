/**************************************************************************************************
  Filename:       knocking.h
  Revised:        $Date: 2014-09-09 10:15:58 +0800 (Tue, 9 Sep 2014) $
  Revision:       $Revision: 0 $

  Description:    Header file for control knocking.When knocking 3 times Phobos
                  will wake up from PM3 to PM2.
**************************************************************************************************/

#ifndef KNOCKING_H
#define KNOCKING_H

#include "hal_types.h"

// Function prototypes
void knockingEnableP0_7(bool flag);

#endif

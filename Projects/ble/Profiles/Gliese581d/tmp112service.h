#ifndef TMP112SERVICE_H
#define TMP112SERVICE_H

#ifdef __cplusplus
extern "C"
{
#endif

    /*********************************************************************
     * INCLUDES
     */

    /*********************************************************************
     * CONSTANTS
     */

    // Profile Parameters
#define TMP112PROFILE_CHAR1                   1  // RW uint8 - Profile Characteristic 1 value


    // Tmp112 Profile Service UUID
    // 0c57270a-30a7-4d7a-a336-7cbe27df2a5b
#define TMP112PROFILE_SERV_UUID 0x5b, 0x2a, 0xdf, 0x27, 0xbe, 0x7c, 0x36, 0xa3, 0x7a, 0x4d, 0xa7, 0x30, 0x0a, 0x27, 0x57, 0x0c

    // 11175c20-831e-4105-a036-f693136745f7
#define TMP112PROFILE_CHAR1_UUID 0xf7, 0x45, 0x67, 0x13, 0x93, 0xf6, 0x36, 0xa0, 0x05, 0x41, 0x1e, 0x83, 0x20, 0x5c, 0x17, 0x11


    // Tmp112 Profile Services bit fields
#define TMP112PROFILE_SERVICE               0x00000001


#define TMP112PROFILE_CHAR1_LEN           6 // The last 4 bytes is passcode


    /*********************************************************************
     * TYPEDEFS
     */


    /*********************************************************************
     * MACROS
     */

    /*********************************************************************
     * Profile Callbacks
     */

    // Callback when a characteristic value has changed
    typedef void (*tmp112ProfileChange_t)( uint8 paramID );

    typedef struct
    {
        tmp112ProfileChange_t        pfnTmp112ProfileChange;  // Called when characteristic value changes
    } tmp112ProfileCBs_t;



    /*********************************************************************
     * API FUNCTIONS
     */


    /*
     * Tmp112Profile_AddService- Initializes the Simple GATT Profile service by registering
     *          GATT attributes with the GATT server.
     *
     * @param   services - services to add. This is a bit map and can
     *                     contain more than one service.
     */

    extern bStatus_t Tmp112Profile_AddService( uint32 services );

    /*
     * Tmp112Profile_RegisterAppCBs - Registers the application callback function.
     *                    Only call this function once.
     *
     *    appCallbacks - pointer to application callbacks.
     */
    extern bStatus_t Tmp112Profile_RegisterAppCBs( tmp112ProfileCBs_t *appCallbacks );

    /*
     * Tmp112Profile_SetParameter - Set a Simple GATT Profile parameter.
     *
     *    param - Profile parameter ID
     *    len - length of data to right
     *    value - pointer to data to write.  This is dependent on
     *          the parameter ID and WILL be cast to the appropriate
     *          data type (example: data type of uint16 will be cast to
     *          uint16 pointer).
     */
    extern bStatus_t Tmp112Profile_SetParameter( uint8 param, uint8 len, void *value );

    /*
     * Tmp112Profile_GetParameter - Get a Simple GATT Profile parameter.
     *
     *    param - Profile parameter ID
     *    value - pointer to data to write.  This is dependent on
     *          the parameter ID and WILL be cast to the appropriate
     *          data type (example: data type of uint16 will be cast to
     *          uint16 pointer).
     */
    extern bStatus_t Tmp112Profile_GetParameter( uint8 param, void *value );


    /*********************************************************************
    *********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* TMP112SERVICE_H */
